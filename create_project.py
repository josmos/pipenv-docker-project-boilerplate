import yaml
from jinja2 import Environment, FileSystemLoader
from pathlib import Path
import os
from subprocess import run

print(Path.cwd())
with open("./config.yaml") as config:
    config = yaml.load(config,Loader=yaml.SafeLoader)

config["ProjectDirectory"] = Path(config['ProjectDirectory']).resolve().joinpath(config['ProjectName'])
print(f"Creating Project in directory: {config['ProjectDirectory']}")
code_dir = config["ProjectDirectory"].joinpath(config['ProjectSubdirName'])
code_dir.mkdir(exist_ok=True, parents=True)
os.symlink(code_dir/".env", config["ProjectDirectory"]/".env")
env = Environment(loader=FileSystemLoader('templates'))


def template_to_file(template, renamefile=False, projectsubdirs=config['ProjectSubdirName']):
    filename = renamefile if renamefile else template
    template = env.get_template(template)
    output = template.render(c=config)
    p = config["ProjectDirectory"].joinpath(projectsubdirs)
    p.mkdir(exist_ok=True)
    with open(p/filename, "w") as fh:
        fh.write(output)


template_to_file("executable.pyi", renamefile=config["ExecutableName"], projectsubdirs=config['ProjectSubdirName'])
template_to_file("main.py")
template_to_file("Dockerfile", projectsubdirs="docker")
template_to_file("requirements.txt", projectsubdirs="docker")

if config["DockerEnvVars"]:
    template_to_file("docker_env", ".env", projectsubdirs=config['ProjectSubdirName'])

code_dir.joinpath("test").mkdir(exist_ok=True)
if not Path.is_file(code_dir/"test/__init__.py"):
    os.mknod(code_dir/"test/__init__.py")
template_to_file("test_main.py", projectsubdirs=f"{config['ProjectSubdirName']}/test")
template_to_file("test_main.py", projectsubdirs=f"{config['ProjectSubdirName']}/test")
for p in config["PythonPackages"]:
    run(["pipenv", "install", p], cwd=config["ProjectDirectory"])
run(["pipenv", "run", "py.test", "--pyargs", config['ProjectSubdirName']], cwd=config["ProjectDirectory"])





