import argparse
import subprocess
from pathlib import Path
from os import getuid

parser = argparse.ArgumentParser(description='')
parser.add_argument("", type=str, metavar="", help="")


# Relative mount Path inside container:

volume_dir = Path(f"/home/josmos/PycharmProjects/pipenv-docker-boilerpalte/projects/{config['ProjectSubdirName']}")

def test_docker():
    """
    Checks if docker is installed. No version check implemented.
    :return:
    """
    print("Testing Docker installation... ", end="")
    stdout = subprocess.run(["docker", "-v"], stdout=subprocess.DEVNULL)
    if stdout.returncode != 0:
        print("\nno Docker executable was found. Please install docker and make sure the Docker-cli is in PATH")
    else:
        print("Done.")


def test_image():
    """
    Checks if the required docker image is available on the client.
    Builds the image if not.
    :return:
    """
    print("Looking for workflow-validator image on Docker client... ", end="")
    stdout = subprocess.run(["docker", "run", "--rm", config["DockerImageName"] , "python", "--version"],
                            stdout=subprocess.DEVNULL,
                            stderr=subprocess.DEVNULL)
    if stdout.returncode != 0:
        print(f"\n {config['DockerImageName']} not found! building..." , end="")
        rtn = subprocess.run(["docker", "build", "-t", {config['DockerImageName']}, "./docker"],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if rtn.returncode != 0:
            with open("error.log", "w") as error_log:
                error_log.write(str(rtn.stdout))
                error_log.write(str(rtn.stderr))
            print("Build Failed! Check error.log for details.")

    print("Done.")


# Parse Commandline arguments:
args = parser.parse_args()


def start_container():
    """
    Starts a python container with all required dependencies
    :return: None
    """
    run_args = ["docker", "run",
                
                "--rm",
               
                "--user={0}:{0}".format(getuid()),
                "-v", "{}/{config['ProjectSubdirName']}:/home/{}".format(Path.cwd(), {{ c.ProjectSubdirName }}, "test_project"),
                "--name", "test_1"
                "--env-file", "docker_env",
                "test_image:latest",
                "python", "test.py"]

    subprocess.call(run_args)


if __name__ == "__main__":
    test_docker()
    test_image()
    start_container()