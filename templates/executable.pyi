import argparse
import subprocess
from pathlib import Path
from main import main
from os import getuid

parser = argparse.ArgumentParser(description='')

parser.add_argument("-d", "--docker", action="store_true",  help="Run in Docker mode")


# Relative mount Path inside container:

volume_dir = Path("{{ c.ProjectDirectory }}/{{ c.ProjectSubdirName }}")

def test_docker():
    """
    Checks if docker is installed. No version check implemented.
    :return:
    """
    print("Testing Docker installation... ", end="")
    stdout = subprocess.run(["docker", "-v"], stdout=subprocess.DEVNULL)
    if stdout.returncode != 0:
        print("\nno Docker executable was found. Please install docker and make sure the Docker-cli is in PATH")
    else:
        print("Done.")


def test_image():
    """
    Checks if the required docker image is available on the client.
    Builds the image if not.
    :return:
    """
    print("Looking for {{ c.DockerImageName }} image on Docker client... ", end="")
    stdout = subprocess.run(["docker", "run", "--rm", "{{ c.DockerImageName }}" , "python", "--version"],
                            stdout=subprocess.DEVNULL,
                            stderr=subprocess.DEVNULL)
    if stdout.returncode != 0:
        print("\n {{ c.DockerImageName }} not found! building... ", end="")
        print(Path.cwd())
        rtn = subprocess.run(["docker", "build", "-t", "{{ c.DockerImageName }}", "{{ c.ProjectDirectory }}/docker"],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if rtn.returncode != 0:
            with open("error.log", "w") as error_log:
                error_log.write(str(rtn.stdout))
                error_log.write(str(rtn.stderr))
            print("Build Failed! Check error.log for details.")

    print("Done.")


# Parse Commandline arguments:
args = parser.parse_args()

def start_container():
    """
    Starts a python container with all required dependencies
    :return: None
    """
    run_args = ["docker", "run",
                {% for arg in c.DockerRunArgs %}
                "{{arg}}",
               {%endfor %}
                "--user={0}:{0}".format(getuid()),
                "-v", "{{ c.ProjectDirectory }}/{{ c.ProjectSubdirName }}:/home/{{ c.ProjectName }}",
                "--name", "{{ c.DockerContainerName }}",
                "--env-file=.env",
                "{{ c.DockerImageName }}",
                "python", "main.py"]
    subprocess.call(run_args)


if __name__ == "__main__":
    if args.docker:
        test_docker()
        test_image()
        start_container()
    else:
        main()