from main import main
from mock import patch


def test_main():
    with patch('sys.exit') as exit_mock:
        main()
        assert exit_mock.called